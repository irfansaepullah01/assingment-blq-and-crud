package com.teama.minpro.teama.model;

public class DetailProfils {
	
	public String fullname;
	
	public String specialization;
	
	public Integer pengalaman;

	public String getFullname() {
		return fullname;
	}

	public void setFullname(String fullname) {
		this.fullname = fullname;
	}

	public String getSpecialization() {
		return specialization;
	}

	public void setSpecialization(String specialization) {
		this.specialization = specialization;
	}

	public Integer getPengalaman() {
		return pengalaman;
	}

	public void setPengalaman(Integer pengalaman) {
		this.pengalaman = pengalaman;
	}
	
	
	

}
