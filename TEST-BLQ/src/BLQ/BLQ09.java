package BLQ;

import java.util.Scanner;

public class BLQ09 {

	public static void main(String[] args) {
		// N = 3 - 3 6 9
		// N = 4 - 4 8 12 16
		// N = 5 - 5 10 15 20 25
		Scanner input = new Scanner(System.in);
		int n = input.nextInt();
		int x = n;
		for(int i = 0; i < n; i++) {
			System.out.print(x + " ");
			x = x + n;
		}
		
		input.close();

	}

}
