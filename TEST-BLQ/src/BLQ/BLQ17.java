package BLQ;

import java.util.Arrays;

public class BLQ17 {

	public static void main(String[] args) {
		// Gunung (Naik dan Turun) dari 0 ke 0 lagi
		// Lembah (Turun dan Naik) dari 0 ke 0 lagi
		// N N T N N N T T T T (T N) T T T N T N
		// 1 2 1 2 3 4 3 2 1 0 (-1 0) -1 -2 -3 -2 -3 -2
		// 1 Gunung dan 1 Lembah

		// Algoritma
		// 1.Membuat input string dan dijadikan array
		// 2.Array di keluarin pake for
		// 3.membuat variavel perjalanan(untuk cek pertambahan Naik dan Pengurangan
		// turun)
		// 4.Logic nya ketika 0 awalan naik dan turun ke 0 itu satu gunung dan ketika 0
		// terus turun maka lembah ketika 0 deui
		// ketika perjalanan mencapai 0 kembali dan terakhir di T itu 1 gunung dan
		// sebaliknya

		String input = "N N T N N N T T T T T N T T T N T N";
		String[] perjalanan = input.split(" ");
		int posisi = 0;
		int gunung = 0;
		int lembah = 0;

		System.out.println(Arrays.toString(perjalanan));

		for (int i = 0; i < perjalanan.length; i++) {
			if (perjalanan[i].equalsIgnoreCase("N")) {
				posisi++;
				if (posisi == 0) {
					System.out.println("posisi lembah = " + perjalanan[i] + " " +i);
					
					lembah++;
				}
			} else if (perjalanan[i].equalsIgnoreCase("T")) {
				posisi--;
				if (posisi == 0) {
					System.out.println("posisi gunung = " +perjalanan[i]+ " " +i);
					gunung++;
				}
			}
		}
		System.out.println(lembah + " Lembah");
		System.out.println(gunung + " Gunung");

	}
}
