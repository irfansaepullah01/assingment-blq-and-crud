package BLQ;

public class BLQ15 {

	public static void main(String[] args) {
		// 15. Ubah format waktu "03:40:44 PM" menjadi format 24 jam (15:40:44)
		// input string jam PM dan AM , ketika pm indeks 0 + 12

		String input = "03:40:44 AM";

		String[] AMPM = input.split(" ");
		String[] waktu = input.split(":");
		int jam = 0;
		String konversi = "";

		for (int i = 0; i < waktu.length; i++) {
			if (AMPM[1].equalsIgnoreCase("PM")) {
				if (i == 0) {
					jam = Integer.valueOf(waktu[i]) + 12;
					konversi += jam + ":";
				} else if (i == 1) {
					konversi += waktu[i] + ":";
				} else {
					konversi += waktu[i];
				}
			} else {
				if(i == waktu.length-1) {
					konversi += waktu[i];
				}
				else {
					konversi += waktu[i] + ":";
				}
			}

		}
		System.out.println(konversi);

	}

}
