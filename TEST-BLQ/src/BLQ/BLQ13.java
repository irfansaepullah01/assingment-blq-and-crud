package BLQ;

public class BLQ13 {

	public static void main(String[] args) {
		// Berapa derajat sudut terkecil yang dibentuk 2 jarum jam?
		// Jam 3:00 - 90
		// Jam 5:30 - 15
		// Jam 2:20 - 50
		// *detik tidak dipertimbangkan

		// input string split by :
		// kondisi jam dikali 30, jika menit konversi ke jam dikali 30
		// kondisi menit, menit dikali 6 konversi 1 jam/60 menit = 360 derajat
		// maka 1 menit = 6 derajat

		String input = "3:45";
		String[] jam = input.split(":");

		Double derajat = 0.0;
		Double jarumPendek = 0.0;
		Double jarumPanjang = 0.0;

		for (int i = 0; i < jam.length; i++) {
			if (i == 0) {
				jarumPendek = (Double.valueOf(jam[i]) * 30); //jam
				jarumPendek += ((Double.valueOf(jam[i + 1]))/60)* 30; //menit konversi ke jam
			} else {
				jarumPanjang = (Double.valueOf(jam[i]) * 6);
			}
		}
		derajat = (Math.abs(jarumPendek - jarumPanjang));
		System.out.println(derajat);
	}

}
