package BLQ;

public class BLQ05 {

	public static void main(String[] args) {
		// 0, 1, 1, 2, 3, 5, 8
		int n = 20;
		int pertama = 0;
		int kedua = 1;
		int hasil = 0;
		int penyimpanx2 = 0;

		for (int i = 0; i < n; i++) {
			if (i == 0) { // i(0) = 0
				System.out.print(pertama + " ");
			} else if (i == 1) { // i(1) = 1
				System.out.print(kedua + " ");
			} else {
				hasil = kedua + pertama; // 1
				System.out.print(hasil + " "); // 1 2
				penyimpanx2 = kedua;
				kedua = hasil;
				pertama = penyimpanx2; // 1 1
			}
		}

	}

}
