package BLQ;

public class BLQ14 {

	public static void main(String[] args) {
		// ROTASI
		// Rotasi sesuai n inputan,
		// 2 for, for 1() print n sampai length-1 || for2() print 0 sampai < n

		String input = "3 9 0 7 1 2 4";
		int nRotasi = 3;
		String[] angka = input.split(" ");

		for (int i = nRotasi; i < angka.length - 1; i++) {
			System.out.print(angka[i] + " ");
		}
		
		for(int i = 0; i < nRotasi; i++) {
			System.out.print(angka[i] + " ");
		}

	}

}
