package BLQ;

import java.util.Arrays;


public class BLQ04 {

	public static void main(String[] args) {
		// 2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71,
		// 73, 79, 83, 89, 97,
		// +1,+2,+2,+4,+2,+4,+2
		// i = 0 --- i = 7;

		// inputan untuk banyak bilangan / length
		// hanya bisa dibagi angka sendiri
		// hanya bisa dibagi 1
		// angka >= 2
		// angka pembagi 2,3,5,7
		// selain dari (2,3,5,7) yang tidak bisa diobagi tersebut

		// array 7
		// hasil = hasil + 1
		// length < 4
		// if(hasil / 2/3/5/7 = 1) = 3
//0		// 2 hardcode
//1		// 2 + 1 = 3 hanya bisa dibagi 3 dan 1
//2		// 3 + 1 = 4 dibagi 2 continue
//3		// 4 + 1 = 5 hanya bisa dibagi 5 dan 1
//4		// 5 + 1 = 6 dibagi 2,3 continue
//5		// 6 + 1 = 7 hanya bisa dibagi 7 dan 1
//6		// 7 + 1 = 8 dibagi 2,4	continue

		int n = 20;
		String[] prima = new String[n];
		int hasil = 2;

		for (int i = 0; i < n; i++) {
			if (i >= 0 && i <= 3) {
				if (((hasil == 2) && hasil % 2 == 0) || ((hasil == 3) && hasil % 3 == 0) 
					|| hasil % 5 == 0|| hasil % 7 == 0) {
					prima[i] = String.valueOf(hasil);
					hasil++;
				} else {
					hasil++;
					i--;
				}
			} else {
				if (hasil % 2 != 0 && hasil % 3 != 0 && hasil % 5 != 0 && hasil % 7 != 0) {
					prima[i] = String.valueOf(hasil);
					hasil++;
				} else {
					hasil++;
					i--;
				}

			}
		}
		System.out.println(Arrays.toString(prima));

//		Scanner input = new Scanner(System.in);
//		int bil, awal, akhir;
//
//		System.out.println("PROGRAM JAVA MENAMPILKAN DERET BILANGAN PRIMA");
//		System.out.print("Mulai dari : ");
//		awal = input.nextInt();
//		System.out.print("Sampai : ");
//		akhir = input.nextInt();
//		System.out.println("----------------------------------------------");
//		for (int i = awal; i <= akhir; i++) {
//			bil = 0;
//			for (int j = 1; j <= i; j++) {
//				if (i % j == 0) {
//					bil = bil + 1;
//				}
//			}
//			if (bil == 2) {
//				System.out.print(i + " ");
//			} else {
//				akhir++;
//			}
//		}
//		input.close();
	}

}
