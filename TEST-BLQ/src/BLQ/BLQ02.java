package BLQ;


import java.util.Arrays;

public class BLQ02 {

	public static void main(String[] args) {
		// PERPUSTAKAAN

		// String[] buku = { "A", "B", "C", "D" };
		int[] hari = { 14, 3, 7, 7 };

		String input = "28 2 2016 - 7 3 2016";
		// 0 1 2 3 (-) 4 5 6

		String[] arrInput = input.split(" ");

		int selisihHari = 0;
		int hariBulan = 0;
		int denda = 0;
		System.out.println(Arrays.toString(arrInput));

		// Kalender(mencari hari)
		for (int i = 0; i < arrInput.length; i++) {
			if (i == 0) {
				if (arrInput[1].equalsIgnoreCase(arrInput[5])) {
					selisihHari = Integer.parseInt(arrInput[i + 4]) - Integer.parseInt(arrInput[i]);
					System.out.println(selisihHari);
				} else {
					if (arrInput[2].equalsIgnoreCase("2016")) { // kabisat
						if (Integer.parseInt(arrInput[1]) % 2 == 1 || Integer.parseInt(arrInput[1]) == 8) { // bulan
							hariBulan = 31;
							selisihHari = hariBulan - Integer.parseInt(arrInput[i]) + Integer.parseInt(arrInput[i + 4]);
							System.out.println(selisihHari);
						} else if (Integer.valueOf(arrInput[1]) == 2) {
							hariBulan = 29;
							selisihHari = hariBulan - Integer.parseInt(arrInput[i]) + Integer.parseInt(arrInput[i + 4]);
							System.out.println(selisihHari);
						} else {
							hariBulan = 30;
							selisihHari = hariBulan - Integer.parseInt(arrInput[i]) + Integer.parseInt(arrInput[i + 4]);
							System.out.println(selisihHari);
						}

					} else {
						if (Integer.parseInt(arrInput[1]) % 2 == 1 || Integer.parseInt(arrInput[1]) == 8) { // bulan
							hariBulan = 31;
							selisihHari = hariBulan - Integer.parseInt(arrInput[i]) + Integer.parseInt(arrInput[i + 4]);
							System.out.println(selisihHari);
						} else if (Integer.valueOf(arrInput[1]) == 2) {
							hariBulan = 28;
							selisihHari = hariBulan - Integer.parseInt(arrInput[i]) + Integer.parseInt(arrInput[i + 4]);
							System.out.println(selisihHari);
						} else {
							hariBulan = 30;
							selisihHari = hariBulan - Integer.parseInt(arrInput[i]) + Integer.parseInt(arrInput[i + 4]);
							System.out.println(selisihHari);
						}

					}

				}

			}

		}

		// Kalkulasi penghitungan denda
		for (int i = 0; i < hari.length; i++) {
			if (hari[i] > selisihHari) {
				denda = (hari[i] - selisihHari) * 100;
				System.out.println(hari[i] + " hari");
				System.out.println(denda);
			}
		}

	}

//		LocalDate awalPeminjaman = LocalDate.of(2016, 02, 28);
//		LocalDate akhirPeminjaman = LocalDate.of(2018, 03, 07);
//	 
//	        Period diff = Period.between(awalPeminjaman, akhirPeminjaman);
//	        
//	 
//		System.out.printf("\nDifference is %d years, %d months and %d days old\n\n", 
//	                diff.getYears(), diff.getMonths(), diff.getDays() + 1 );
}
