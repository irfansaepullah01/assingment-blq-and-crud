package BLQ;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

public class BLQ03 {

	public static void main(String[] args) {
		int tarif = 0;
		LocalDateTime masuk = LocalDateTime.parse("2019-01-27T05:00:01");

		LocalDateTime keluar = LocalDateTime.parse("2019-01-27T17:45:03");
		
		int jam = (int) ChronoUnit.HOURS.between(masuk, keluar);
		System.out.println(jam);
		
		if(jam <= 8) {
			tarif = jam * 1000;
		}
		else if (jam > 8 && jam <= 24) {
			tarif = 8000 + ((jam - 8) * 1000);
		}
		else {
			tarif = 15000 + ((jam - 24) * 1000);
		}
		
		System.out.println("tarif parkir selama " + jam + " jam adalah = " + tarif);
		
		
		
		
	}

}
