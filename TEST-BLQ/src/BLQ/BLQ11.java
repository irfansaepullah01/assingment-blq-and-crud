package BLQ;

public class BLQ11 {

	public static void main(String[] args) {
		// Input: Jeruk
		/*
		 * Output: k** u** r** e** J
		 ***/
		// Input string, split karakter, print dari belakang
		// bintang?

		String input = "JerukBali";
		String[] karakter = input.split("");
		for (int i = karakter.length - 1; i >= 0; i--) { //i
			for (int j = 0; j < karakter.length; j++) { //
				if (j == input.length() / 2) {
					System.out.print(karakter[i]);
				} else {
					System.out.print("*");
				}
			}
			System.out.println();
		}

	}

}
