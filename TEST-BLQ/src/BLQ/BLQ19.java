package BLQ;

import java.util.Arrays;

public class BLQ19 {

	public static void main(String[] args) {
		// *Pangram adalah kata atau kalimat yang mengandung setiap abjad alphabet,
		// contohnya “A quick brown fox jumps over the lazy dog”

		String input = "A quick brown fox jumps over the lazy dog".replace(" ", "").toLowerCase();
		String alfabet = "abcdefghijklmnopqrstuvwxyz";

		String[] kata = input.split("");
		String[] abjad = alfabet.split("");
		
		Arrays.sort(kata);
		
		int jumlah = 0;
		String pembanding = "";

		System.out.println(Arrays.toString(kata));
		System.out.println(Arrays.toString(abjad));

		System.out.println(abjad.length);

		for (int i = 0; i < kata.length; i++) {
			for (int j = 0; j < abjad.length; j++) {
				if(i == 0) {
					if(kata[i].equals(abjad[j])) {
						jumlah++;
						pembanding = kata[i]; //a
					}	
				}
				else {
					if(kata[i].equals(abjad[j])) {		//a = a
						if(pembanding.equals(kata[i])) { //a == a
							continue;
						}else {
							jumlah++;
							pembanding = kata[i]; //a
						}		
					}

				}
				if(kata[i].equals(abjad[j])) {
					pembanding = kata[i];			//a = a
					if(pembanding.equals(kata[i])) { //a == a
						continue;
					}else {
						jumlah++;
					}		
				}
			}
		}
		
		System.out.println(jumlah);

	}

}
