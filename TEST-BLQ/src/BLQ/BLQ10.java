package BLQ;

import java.util.Arrays;

public class BLQ10 {

	public static void main(String[] args) {
		// Input Output
		// Susilo Bambang Yudhoyono S***o B***g Y***o
		// Rani Tiara R***i T***a

		// Membuat inputan string, kemudian di split per kalimat
		// Membuat for 1 untuk kalimat for 2 untuk setiap kata dalam kalimat
		// Mengkondisikan indeks awal dan akhir print huruf split, selain itu print
		// bintang

		String input = "Susilo";
		String[] kalimat = input.split(" ");

		System.out.println(Arrays.toString(kalimat));

		for (int i = 0; i < kalimat.length; i++) {
			for (int j = 0; j < kalimat[i].length(); j++) {
				if (j == 0 || j == kalimat[i].length() - 1) {
					System.out.print(kalimat[i].charAt(j));
				} else {
					System.out.print("*");
				}
			}
			System.out.print(" ");

		}

	}

}
