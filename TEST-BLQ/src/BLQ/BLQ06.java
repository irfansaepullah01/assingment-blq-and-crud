package BLQ;

import java.util.Arrays;

public class BLQ06 {

	public static void main(String[] args) {
		// PALINDROME
		// Kasur ini rusak Kasur Nababan rusak Katak Malam Radar
		// Cek kata? Palindrome atau bukan (berati perlu kondisi ketika palindrome atau
		// bukan)
		// Kondisi = (Kata awal == Kata akhir) cari nilai tengah nya juga
		//

		// System.out.println(9/2); sama dengan median

		String input = "Kasur ini rusak".replace(" ", "").trim();
		String[] kata = input.split("");
		int syarat = 0;

		System.out.println(kata.length - 1);
		System.out.println(Arrays.toString(kata));

		for (int i = 0; i < kata.length; i++) {
			if (kata[i].equalsIgnoreCase(kata[(kata.length - 1) - i])) {
				syarat++;
				System.out.println(kata[i] + " = " + kata[(kata.length - 1) - i]);
			}
		}

		if (syarat == kata.length) {
			System.out.println("Kalimat " + input + " adalah palindrome");
		}
		else {
			System.out.println("Kalimat " + input + " bukan termasuk palindrome");
		}

	}

}
