package BLQ;

import java.util.Arrays;

public class BLQ21 {

	public static void main(String[] args) {
		// Lintasan (Walk & Jump)
		// tujuan? menentukan walk and jump dari lintasan
		// pasti jalan untuk isi stamina ketika memenuhi lewatin index baru bisa jump
		// _ _ _ _ _ O _ _ _ Finish
		// mencari length o berapa ? dikurangi distance

		// d nya berapa index nya berapa biar bisa melewati
		/*
		 * - Walk : ST +1, D +1 - Jump : ST -2, D +3
		 */

		// string[] lintasan = {"_", "_", "_", "_", "_", "O", "_", "_", "_","Finish"};
		String[] lintasan = {"_","_","_","_","_","O","_","_", "_","Finish"};
		System.out.println(Arrays.toString(lintasan));

		int ST = 0;
		int D = 0;

		for (int i = 0; i < lintasan.length; i++) {

			if (i == 0 && lintasan[i] == "O") {
				System.out.print("FAILED!");
				break;
			} else {
				if (ST > 3) {
					if (lintasan[i + 3] != "0" && i < lintasan.length - 3) {
						ST -= 2;
						D += 3;
						// i += 3;

						System.out.print("J, ");

						if (ST > 1) {
							ST -= 2;
							D += 3;
							// i += 3;

							System.out.print("J, ");
						}
					} else {
						ST += 1;
						D += 1;

						System.out.print("W, ");
					}
				} else {
					ST += 1;
					D += 1;

					System.out.print("W, ");
				}

				if (D >= lintasan.length - 1) {
					break;
				}

			}

		}
	}
}
