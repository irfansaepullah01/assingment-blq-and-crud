package BLQ;

import java.util.Arrays;

public class BLQ08 {

	public static void main(String[] args) {
		//MINIMAL & MAKSMIMAL
		/*Tentukan nilai minimal dan maksimal 
		 * dari penjumlahan 4 komponen deret ini 1 2 4 7 8 6 9*/
		
		String input = "1 2 4 7 8 6 9";
		String[] angka = input.split(" ");
		int minimal = 0;
		int maksimal = 0;
		int n = 4;

		Arrays.sort(angka);
		// System.out.println(Arrays.toString(angka));

		// minimal
		for (int i = 0; i < n; i++) {
			minimal += Integer.valueOf(angka[i]);
		}
		System.out.println("Nilai minimal = " + minimal);

		// maksimal
		for (int i = angka.length - 1; i >= n - 1; i--) {
			maksimal += Integer.valueOf(angka[i]);
		}
		System.out.println("Nilai maksimal = " + maksimal);

	}

}
