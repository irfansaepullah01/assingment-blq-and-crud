--RUMAH MAKAN
SELECT * FROM rumah_makan

DROP TABLE rumah_makan
CREATE TABLE rumah_makan
(
	id bigint generated always as identity primary key not null,
	menu varchar(50) not null, 
	pembeli varchar(50) not null,
	harga decimal (28,3) not null,
	created_by varchar(50) not null,  
	created_on timestamp not null,
	modified_by varchar(50) null,
	modified_on timestamp null,
	deleted_by varchar(50) null,
	deleted_on timestamp null,
	is_delete boolean not null default false
)

INSERT INTO rumah_makan (menu,pembeli,harga,created_by,created_on)
(
VALUES	('Tempe Goreng','Mba Cindy',3000,'RM Mitoha',now())
)